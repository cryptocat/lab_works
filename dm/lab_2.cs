using System;

namespace dml_2
{
	class Program
	{
		public static bool Reflective(int[,] matrix, int n)
		{
			for (int i = 0; i < n; i++)
			{
				if (matrix[i, i] == 0)
				{
					return false;
				}
			}
			return true;
		}

		public static bool Symmetry(int[,] matrix, int n)
		{
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if (matrix[i, j] != matrix[j, i])
					{
						return false;
					}
				}
			}
			return true;
		}

		public static bool Antysimmetry(int[,] matrix, int n)
		{
			int[,] _matrix = new int[n, n];
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					_matrix[i, j] = matrix[i, j] * matrix[j, i];
				}
			}
			for (int i = 0; i < n; i++)
			{
				if (_matrix[i, i] != 1)
				{
					return false;
				}
			}
			return true;
		}

		public static bool Trnstvn_vld(int[,] matrix, int n)
		{
			int[,] _matrix = new int[n, n];
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					_matrix[i, j] = 0;
					for (int k = 0; k < n; k++)
					{
						_matrix[i, j] += matrix[i, k] * matrix[k, j];
						if (_matrix[i, j] >= 1)
						{
							_matrix[i, j] = 1;
						}
					}
					Console.Write(_matrix[i, j] + " ");
				}
				Console.WriteLine();
			}
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if (matrix[i, j] == _matrix[i, j])
					{
						return false;
					}
				}
			}
			return true;
		}
		static void Print(int[,] a)
		{
			for (int i = 0; i < a.GetLength(0); i++)
			{
				for (int j = 0; j < a.GetLength(1); j++)
				{
					Console.Write("{0} ", a[i, j]);
				}
				Console.WriteLine();
			}
		}
		static void Main(string[] args)
		{
			Console.Write("Enter n = ");
			int n = int.Parse(Console.ReadLine());

			Random NumRand = new Random();
			int[,] matrix1 = new int[n, n];

			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					matrix1[i, j] = NumRand.Next(0, 2);
				}
			}
			Console.WriteLine("Matrix A");
			Print(matrix1);
			
			if (Reflective(matrix1, n))
			{
				Console.WriteLine("Is reflective");
			}
			else
			{
				Console.WriteLine("Is not reflective");
			}
			if (Symmetry(matrix1, n))
			{
				Console.WriteLine("Is symmetrical");
			}
			else
			{
				Console.WriteLine("Is not symmetrical");
			}
			if (Antysimmetry(matrix1, n))
			{
				Console.WriteLine("Is antisimmetrical");
			}
			else
			{
				Console.WriteLine("Is not antisimmetrical");
			}
			if (Trnstvn_vld(matrix1, n))
			{
				Console.WriteLine("Is transitive");
			}
			else
			{
				Console.WriteLine("Is not transitive");
			}
			Console.ReadKey();
		}
	}
}